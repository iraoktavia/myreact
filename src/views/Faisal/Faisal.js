import React, { Component } from 'react';
import { Bar, Line } from 'react-chartjs-2';
import {
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Row,
  Table,
} from 'reactstrap';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import Pagination from "react-js-pagination";
const brandPrimary = getStyle('--primary')
const brandSuccess = getStyle('--success')
const brandInfo = getStyle('--info')
const brandWarning = getStyle('--warning')
const brandDanger = getStyle('--danger')

//Random Numbers
function random(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

var elements = 27;
var data1 = [];

for (var i = 0; i <= elements; i++) {
  data1.push(random(50, 200));
}

//Bar Chart
const bar = {
  labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
  datasets: [
    {
      label: 'My First dataset',
      backgroundColor: 'rgba(255,99,132,0.2)',
      borderColor: 'rgba(255,99,132,1)',
      borderWidth: 1,
      hoverBackgroundColor: 'rgba(255,99,132,0.4)',
      hoverBorderColor: 'rgba(255,99,132,1)',
      data: [65, 59, 80, 81, 56, 55, 40],
    },
  ]
};

const options = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips
  },
  maintainAspectRatio: false
}

// Main Chart
const mainChart = {
  labels: ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'],
  datasets: [
        {
          label: 'Sukses',
          fill: false,
          lineTension: 0.1,
          backgroundColor: 'rgba(75,192,192,0.4)',
          borderColor: 'rgba(75,192,192,1)',
          borderCapStyle: 'butt',
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: 'miter',
          pointBorderColor: 'rgba(60,180,180,5)',
          pointBackgroundColor: '#000',
          pointBorderWidth: 1,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: 'rgba(75,192,192,1)',
          pointHoverBorderColor: 'rgba(220,220,220,1)',
          pointHoverBorderWidth: 2,
          pointRadius: 1,
          pointHitRadius: 10,
          data: [65, 59, 80, 81, 56, 55, 40],
        },
        {
          label: 'Gagal',
          fill: false,
          lineTension: 0.1,
          backgroundColor: 'rgba(75,192,192,0.4)',
          borderColor: 'rgba(255,99,132,1)',
          borderCapStyle: 'butt',
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: 'miter',
          pointBorderColor: 'rgba(75,192,192,1)',
          pointBackgroundColor: '#000',
          pointBorderWidth: 1,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: 'rgba(75,192,192,1)',
          pointHoverBorderColor: 'rgba(220,220,220,1)',
          pointHoverBorderWidth: 2,
          pointRadius: 1,
          pointHitRadius: 10,
          data: [65, 59, 80, 81, 56, 55, 40],
        }
    ]
};

const mainChartOpts = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips,
    intersect: true,
    mode: 'index',
    position: 'nearest',
    callbacks: {
      labelColor: function(tooltipItem, chart) {
        return { backgroundColor: chart.data.datasets[tooltipItem.datasetIndex].borderColor }
      }
    }
  },
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  scales: {
    xAxes: [
      {
        gridLines: {
          drawOnChartArea: false,
        },
      }],
    yAxes: [
      {
        ticks: {
          beginAtZero: true,
          maxTicksLimit: 5,
          stepSize: Math.ceil(4000 / 10),
          max: 4000,
        },
      }],
  },
  elements: {
    point: {
      radius: 0,
      hitRadius: 10,
      hoverRadius: 4,
      hoverBorderWidth: 3,
    },
  },
};

class Faisal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      mainChart: mainChart,
      bar: bar,
      activePage: 0,
      tabel: {
          "data":[],
          "totalRows": 0
        }
    };
    this.loadDataTable = this.loadDataTable.bind(this);
  }

  componentWillMount() {
    this.loadBarData();
    this.loadChartData();
    this.loadDataTable(1);
  }

  loadBarData(){
    let x =Object.assign({},bar);
    let parseResponse = function(response){
      return response.json();
      }

    fetch('http://localhost:5000/faisal/succed',{
      method: 'GET',
      body:null,
      })
      .then(parseResponse)
      .then((response)=> {
        console.log(response);
        x["labels"] = response["labels"];
        x["datasets"][0]["data"] = response["data"];
        this.setState({
          bar : x
        });
      }); 
  }

  loadChartData(){
    let y = Object.assign({},mainChart);
    let parseResponse = function(response){
      return response.json();
      }

    fetch('http://localhost:5000/faisal/get',{
      method: 'GET',
      body:null,
      })
      .then(parseResponse)
      .then((response)=> {
        console.log(response);
        y["labels"] = response["labels"];
        y["datasets"][0]["data"] = response["data"]["sukses"];
        y["datasets"][1]["data"] = response["data"]["gagal"];

        this.setState({
          mainChart : y
        });
      }); 
  }

  loadDataTable(pNumber){
    let parseResponse = function(response){
      return response.json();
      }
    fetch('http://localhost:5000/faisal/',{
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        limit: 5,
        pageNumber: pNumber
        })
      })
      .then(parseResponse)
      .then((response)=> {
        console.log(response);
        this.setState({
          tabel: response,
          activePage: pNumber
        });
      });  
  }  

  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

  render() {

    return (
      <div className="animated fadeIn">
        <Row>
          <Col sm="6">
            <Card>
                <CardHeader>
                  <CardTitle className="mb-0">Bar Chart Succed Transactions</CardTitle>
                    <div className="card-header-actions"></div>
                  </CardHeader>
              <CardBody>
                <br/><br/>
                <div className="chart-wrapper">
                  <Bar data={this.state.bar} options={options}/>
                </div>
              </CardBody>
              <br/><br/>
            </Card>
          </Col>
          <Col sm="6">
            <Card>
              <CardHeader>
                <CardTitle className="mb-0">Line Chart Succed and Failed Transactions</CardTitle>
                    <div className="card-header-actions"></div>
              </CardHeader>
              <CardBody>
                  <div className="chart-wrapper" style={{ height: 300 + 'px', marginTop: 40 + 'px' }}>
                  <Line data={this.state.mainChart} options={mainChartOpts} height={300} />
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col>
            <Card>
              <CardHeader>
                <CardTitle className="mb-0">Table with Pagination</CardTitle>
                <div className="card-header-actions"></div>
              </CardHeader>
              <CardBody>
                <br />
                <Table hover responsive className="table-outline mb-0 d-none d-sm-table">
                  <thead className="thead-light">
                  <tr>
                    <th className="text-center">NO</th>
                    <th className="text-center">NO. REKENING</th>
                    <th className="text-center">JENIS KARTU</th>
                    <th className="text-center">TIPE CHANNEL</th>
                    <th className="text-center">JENIS TRANSAKSI</th>
                    <th className="text-center">STATUS</th>
                    <th className="text-center">JUMLAH</th>
                    <th className="">TANGGAL</th>
                  </tr>
                  </thead>
                  <tbody>
                    {this.state.tabel["data"].map((row, index) => {
                      return (
                        <tr key={index}>
                          <td>{index + 1 +(this.state.activePage-1)*5}</td>
                          <td>{row.norek}</td>
                          <td>{row.jenis_kartu}</td>
                          <td>{row.tipe_channel}</td>
                          <td>{row.jenis_trx}</td>
                          <td>{row.status_code}</td>
                          <td>{row.trx_amount}</td>
                          <td>{row.timestamp}</td>
                        </tr>
                      )
                    })}
                  </tbody>
                </Table>
                <Pagination itemClass="page-item" linkClass="page-link" activePage={this.state.activePage} itemsCountPerPage={10}
                  totalItemsCount={this.state.tabel.totalRows} onChange={this.loadDataTable}/>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Faisal;