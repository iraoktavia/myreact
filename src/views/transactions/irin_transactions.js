import React, {Component} from 'react';
import { Bar, Line} from 'react-chartjs-2';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import Pagination from "react-js-pagination";
import {
  Row,
  Col,
  Card,
  CardBody,
  Table,
  CardHeader,
} from 'reactstrap';
import { stringify } from 'querystring';

const line = {
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    datasets: [
      {
        label: 'Succeed Transaction',
        fill: false,
        lineTension: 0.1,
        backgroundColor: 'rgba(75,192,192,0.4)',
        borderColor: 'rgba(75,192,192,1)',
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: 'rgba(75,192,192,1)',
        pointBackgroundColor: '#fff',
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: 'rgba(75,192,192,1)',
        pointHoverBorderColor: 'rgba(220,220,220,1)',
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: [65, 59, 80, 81, 56, 55, 40],
      },
      {
        label: 'Failed Transaction',
        fill: false,
        lineTension: 0.1,
        backgroundColor: 'rgba(74,192,192,0.4)',
        borderColor: 'rgba(74,192,192,1)',
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: 'rgba(74,192,192,1)',
        pointBackgroundColor: '#fff',
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: 'rgba(74,192,192,1)',
        pointHoverBorderColor: 'rgba(221,220,220,1)',
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: [100, 200, 250, 300, 310, 320, 330],
      },
    ],
  };
  
  const bar = {
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    datasets: [
      {
        label: 'Failed Transaction Frequency',
        backgroundColor: 'rgba(255,99,132,0.2)',
        borderColor: 'rgba(255,99,132,1)',
        borderWidth: 1,
        hoverBackgroundColor: 'rgba(255,99,132,0.4)',
        hoverBorderColor: 'rgba(255,99,132,1)',
        data: [65, 59, 80, 81, 56, 55, 40],
      },
    ],
  };

  const options = {
    tooltips: {
      enabled: false,
      custom: CustomTooltips
    },
    maintainAspectRatio: false
  }

  class Charts extends Component {
    constructor(props){
      super(props);
      
      this.loadTransactionData=this.loadTransactionData.bind(this)
      
      this.state={
        transactionData: {
            data : [],
            totalRows : 0,
        },
        dataBar : bar,
        dataLine : line, 
        activePage : 1,
      } 
    }
    componentDidMount(){
      this.loadTransactionDataBar()
      this.loadSucceedFailData()
      this.loadTransactionData(1)
    }

    loadSucceedFailData() {
      fetch('http://localhost:5000/irin/sumhour', {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: null
      }).then((response) => {
      return response.json()
      })
      .then((data) => {
        const line = {
          labels: [],
          datasets: [
            {
              label: 'Succeed Transaction',
              fill: false,
              lineTension: 0.1,
              backgroundColor: 'rgba(75,192,192,1)',
              borderColor: 'rgba(75,192,192,1)',
              borderCapStyle: 'butt',
              borderDash: [],
              borderDashOffset: 0.0,
              borderJoinStyle: 'miter',
              pointBorderColor: 'rgba(75,192,192,1)',
              pointBackgroundColor: '#fff',
              pointBorderWidth: 1,
              pointHoverRadius: 5,
              pointHoverBackgroundColor: 'rgba(75,192,192,1)',
              pointHoverBorderColor: 'rgba(220,220,220,1)',
              pointHoverBorderWidth: 2,
              pointRadius: 1,
              pointHitRadius: 10,
              data: [],
            },
            {
              label: 'Failed Transaction',
              fill: false,
              lineTension: 0.1,
              backgroundColor: '#F08080',
              borderColor: '#F08080',
              borderCapStyle: 'butt',
              borderDash: [],
              borderDashOffset: 0.0,
              borderJoinStyle: 'miter',
              pointBorderColor: '#F08080',
              pointBackgroundColor: '#fff',
              pointBorderWidth: 1,
              pointHoverRadius: 5,
              pointHoverBackgroundColor: 'rgba(74,192,192,1)',
              pointHoverBorderColor: 'rgba(221,220,220,1)',
              pointHoverBorderWidth: 2,
              pointRadius: 1,
              pointHitRadius: 10,
              data: [],
            },
          ],
        };
        line["labels"] = data["label"]
        line["datasets"][0]["data"] = data["data"]["sukses"]
        line["datasets"][1]["data"] = data["data"]["gagal"]

        this.setState({
          dataLine : line
        })
        console.log(data["label"])
      });
    }

    loadTransactionDataBar() {
      fetch('http://localhost:5000/irin/failed/trans', {
      method: 'GET',
      headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
      },
      body: null
    }).then((response) => {
    return response.json()
    })
    .then((data) => {
      let bar = {
        labels: [],
        datasets: [
          {
            label: 'Failed Transaction Frequency',
            backgroundColor: 'rgba(255,99,132,0.2)',
            borderColor: 'rgba(255,99,132,1)',
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(255,99,132,0.4)',
            hoverBorderColor: 'rgba(255,99,132,1)',
            data: [],
          },
        ],
      };
      bar["labels"] = data["labels"]
      bar["datasets"][0]["data"] = data["data"]

      this.setState({
        dataBar : bar
      })
      console.log(data["labels"])
    });
  }

  loadTransactionData(pageNumber) {
    this.setState({
        activePage: pageNumber
    })
    fetch('http://localhost:5000/irin/read/trans', {
      method: 'POST',
      headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        limit : 5,
        page : pageNumber,
      })
    }).then((response) => {
    return response.json()
    })
    .then((data) => {
      this.setState({
        transactionData: data,
      })
      
    })
  }
    render() {
      return (
        <div className="animated fadeIn">
          <Row>
            <Col>
            <Card>
              <CardBody>
              <div className="chart-wrapper">
                <Bar data={this.state.dataBar} options={options} />
              </div>
              </CardBody>
            </Card>
            </Col>
            <Col>
            <Card>
            <CardBody>
              <div className="chart-wrapper">
                <Line data={this.state.dataLine} options={options} /> 
              </div>
            </CardBody>
            </Card>
            </Col>
          </Row>
            <Col>
            <Card>
                <CardHeader>
                    Data Transaction
                </CardHeader>    
              <CardBody>
                <Table hover responsive className="table-outline mb-0 d-none d-sm-table">
                  <thead className="thead-light">
                    <tr>
                      <th className="text-center">No</th>
                      <th className="text-center">Norek</th>
                      <th className="text-center">Tipe Channel</th>
                      <th className="text-center">Jenis Transaksi</th>
                      <th className="text-center">Status Code</th>
                      <th className="text-center">Jumlah Transaksi</th>
                      <th className="text-center">Timestamp</th>
                      <th className="text-center">Jenis Kartu</th>
                    </tr>
                  </thead>
                  <tbody>
                    { this.state.transactionData.data.map((row, index) => {
                      return (
                        <tr key={index}>
                          <td>{index + 1}</td>
                          <td>{row.norek}</td>
                          <td>{row.tipe_channel}</td>
                          <td>{row.jenis_trx}</td>
                          <td>{row.status_code}</td>
                          <td>{row.trx_amount}</td>
                          <td>{row.timestamp}</td>
                          <td>{row.jenis_kartu}</td>
                          {/* <td><Button color="primary" onClick={() => this.visualize(row.id)}>Visualize</Button></td> */}
                        </tr>
                      )
                    }) }
                  </tbody>
                </Table>
                <Pagination
                  itemClass="page-item"
                  linkClass="page-link"
                  activePage={this.state.activePage}
                  itemsCountPerPage={5}
                  totalItemsCount={this.state.transactionData.totalRows}
                  onChange={this.loadTransactionData}
                />
              </CardBody>
            </Card>
            </Col>
          <Row>
          </Row>
          </div>
      )
    }
  }

  
  export default Charts;
  