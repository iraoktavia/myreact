import React, { Component, lazy, Suspense } from 'react';
import { Bar, Line } from 'react-chartjs-2';
import {
  Badge,
  Button,
  ButtonDropdown,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Form,
  FormGroup,
  FormText,
  FormFeedback,
  Input,
  Label,
  Progress,
  Row,
  Table,
} from 'reactstrap';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import Pagination from "react-js-pagination";

const brandPrimary = getStyle('--primary')
const brandSuccess = getStyle('--success')
const brandInfo = getStyle('--info')
const brandWarning = getStyle('--warning')
const brandDanger = getStyle('--danger')


const bar = {
  labels: ['January', 'February', 'March', 'April'],
  datasets: [
    {
      label: 'Success Transactions',
      backgroundColor: 'skyblue',
      borderColor: 'darkblue',
      borderWidth: 1,
      hoverBackgroundColor: 'rgba(255,99,132,0.4)',
      hoverBorderColor: 'rgba(255,99,132,1)',
      data: [65, 59, 80, 81],
    },
  ],
};

const options = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips
  },
  maintainAspectRatio: false
}

function random(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

var elements = 27;
var data1 = [];
var data2 = [];
var data3 = [];

for (var i = 0; i <= elements; i++) {
  data1.push(random(50, 200));
  data2.push(random(80, 100));
  data3.push(65);
}

const mainChart = {
  labels: [],
  datasets: [
    {
      label: 'Sukses',
      fill: false,
      lineTension: 0.1,
      backgroundColor: 'rgba(75,192,192,0.4)',
      borderColor: 'rgba(75,192,192,1)',
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: 'rgba(75,192,192,1)',
      pointBackgroundColor: '#fff',
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: 'rgba(75,192,192,1)',
      pointHoverBorderColor: 'rgba(220,220,220,1)',
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data: [],
    },
    {
      label: 'Gagal',
      fill: false,
      lineTension: 0.1,
      backgroundColor: 'red',
      borderColor: 'red',
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: 'red',
      pointBackgroundColor: '#fff',
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: 'red',
      pointHoverBorderColor: 'rgba(220,220,220,1)',
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data: [],
    },
  ],
};


const optionsLine = {
    tooltips: {
        enabled: false,
        custom: CustomTooltips
    },
    maintainAspectRatio: false
}

class Anissa extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.loadBarChart = this.loadBarChart.bind(this);
    this.loadTrxPagination = this.loadTrxPagination.bind(this);

    this.state = {
      dropdownOpen: false,
      line: mainChart ,
      bar: bar,
      trxPagination: {totalRows:0, data:[]},
      activePage: 1,
      jekar: '',
      tipchan: '',
      awal: '',
      akhir: '',
    };
  }

  componentDidMount() {
    this.loadLineChart();
    this.loadTrxPagination(1);
    this.loadBarChart();
  }

  parseResponse(response){
      return response.json();
  }

  onChange = e => this.setState({ [e.target.name]: e.target.value })

  loadLineChart() {
    let data = Object.assign({}, this.state.line);
    
    let handleRespone = function(response){
          
      return response.json()
    }
    fetch('http://localhost:5000/anissa/perJam', {
      method: 'GET' ,
      headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
      body: null
    })
    .then(handleRespone)
    .then((x) => {
      data["labels"] = x["labels"];
      data["datasets"][0]["data"] = x["data"]["sukses"];
      data["datasets"][1]["data"] = x["data"]["gagal"];
      this.setState({
        line : data
      });
      console.log(data)
    });

  }

  loadBarChart(type) {
    let x = Object.assign({}, bar);
    let handleRespone = function(response){
      console.log(response);
      x["labels"] = response["labels"];
      x["datasets"][0]["data"] = response["data"];
      return x;
    }
    fetch('http://localhost:5000/anissa/trxBerhasil', {
      method: 'GET' ,
      body: null
    })
    .then(this.parseResponse)
    .then(handleRespone)
    .then((x) => {
      this.setState({
        bar : x
      });
    });
  }

  loadTrxPagination(hlmn) {
    this.setState({
      activePage: hlmn
    })
    let handleRespone = function(response){
      return response.json();
    }
    fetch('http://localhost:5000/anissa/pagination', {
      method: 'POST' ,
      headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
      body: JSON.stringify({
        jekar: this.state.jekar,
        tipchan: this.state.tipchan,
        awal: this.state.awal,
        akhir: this.state.akhir,
        limit: 5,
        pageNumbers: hlmn,
      }),
    })
    .then(handleRespone)
    .then((response) => {
      this.setState({
        trxPagination : response,
      });
    });
  }

  

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
    });
  }



  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

  render() {

    return (
      <div className="animated fadeIn">
        <Row>
          <Col sm="6">
            <Card>
              <CardHeader>
                <CardTitle className="mb-0">Transaksi Berhasil</CardTitle>
              </CardHeader>
              <CardBody>
                <div className="chart-wrapper" style={{ height: 300 + 'px', marginTop: 40 + 'px'}}>
                    <Bar data={this.state.bar} options={options} height={300} />
                </div>
                
                
              
              </CardBody>
            </Card>
          </Col>

          <Col sm="6">
            <Card>
              <CardHeader>
                <CardTitle className="mb-0">Transaksi tanggal 12-01-2019</CardTitle>
              </CardHeader>
              <CardBody>
                <div className="chart-wrapper" style={{ height: 300 + 'px', marginTop: 40 + 'px'}}>
                  <Line data={this.state.line} options={optionsLine} height={300}/>
                </div>
                <br/><br/>
                
                
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Row>
          <Col>
            <Card>
              <CardHeader>
                Silahkan Pilih : 
              </CardHeader>
              <CardBody>
                <FormGroup row>
                  <Col xs="3">
                    <Label >Jenis Kartu</Label>
                  </Col>
                  <Col xs="9">
                    <Input type="select" name="jekar" id="jekar" value={this.state.jekar} onChange={this.onChange}>
                          <option value="">Pilih Jenis Kartu :</option>
                          <option value="BRITAMA">BRITAMA</option>
                          <option value="SIMPEDES">SIMPEDES</option>
                          <option value="PRIORITAS">PRIORITAS</option>
                    </Input>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs="3">
                    <Label >Tipe Channel</Label>
                  </Col>
                  <Col xs="9">
                    <Input type="select" name="tipchan" id="tipchan" value={this.state.tipchan} onChange={this.onChange}>
                          <option>Pilih Tipe Channel :</option>
                          <option value="ATM">ATM</option>
                          <option value="EDC">EDC</option>
                          <option value="SMS BANKING">SMS BANKING</option>
                          <option value="INTERNET BANKING">INTERNET BANKING</option>
                    </Input>
                  </Col>
                </FormGroup>
                <FormGroup row>
                    <Col xs="3">
                      <Label htmlFor="time">Periode</Label>
                    </Col>
                    <Col xs="4">
                       
                      <Input type="date" name="awal" id="awal" value={this.state.awal} onChange={this.onChange} required />
                    </Col>
                    <Col xs="1">
                      <Label>s/d</Label>
                    </Col>
                    <Col xs="4">
                       
                      <Input type="date" name="akhir" id="akhir" value={this.state.akhir} onChange={this.onChange} required />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Col xs="4"></Col>
                    <Col xs="4">
                        <Button type="button" size="sm" color="primary" onClick={() => this.loadTrxPagination(this.state.activePage)}><i className="fa fa-dot-circle-o"></i> Request</Button>
                    <Col xs="4"></Col>
                    </Col>
                  </FormGroup>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Row>
          <Col>
            <Card>
              <CardHeader>
                Table of Transactions
              </CardHeader>
              <CardBody>
                <Table hover responsive className="table-outline mb-0 d-none d-sm-table">
                  <thead className="thead-light">
                    <tr>
                      <th className="text-center">No.</th>
                      <th className="text-center">Jenis Kartu</th>
                      <th className="text-center">Jenis Transaksi</th>
                      <th className="text-center">Nomor Rekening</th>
                      <th className="text-center">Status Code</th>
                      <th className="text-center">Timestamp</th>
                      <th className="text-center">Tipe Channel</th>
                      <th className="text-center">Amount</th>
                    </tr>
                  </thead>
                  <tbody>
                    { this.state.trxPagination.data.map((row, index) => {
                      return (
                          <tr key={index}>
                            <td className="text-center">{index + 1}</td>
                            <td className="text-center">{row.jenis_kartu}</td>
                            <td className="text-center">{row.jenis_trx}</td>
                            <td className="text-center">{row.norek}</td>
                            <td className="text-center">{row.status_code}</td>
                            <td className="text-center">{row.timestamp}</td>
                            <td className="text-center">{row.tipe_channel}</td>
                            <td className="text-center">{row.trx_amount}</td>
                          </tr>
                        )
                    }) }
                  </tbody>
                </Table>
                <Pagination
                  itemClass="page-item"
                  linkClass="page-link"
                  activePage={this.state.activePage}
                  itemsCountPerPage={10}
                  totalItemsCount={this.state.trxPagination.totalRows}
                  onChange={this.loadTrxPagination}
                />
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Anissa;

