import React, { Component } from 'react';
import { Bar, Line } from 'react-chartjs-2';
import {
    Button,
    Card,
    CardBody,
    CardHeader,
    Col,
    Row,
    Table,
    CardColumns,
    Form,
    FormGroup,
    Input,
    Progress,
    Label,
    Modal,
    ModalHeader,
    ModalBody,
    FormText,
    ModalFooter,
  } from 'reactstrap';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import Pagination from 'react-js-pagination';
import RcIf, { RcElse } from 'rc-if';

const options = {
    tooltips: {
        enabled: false,
        custom: CustomTooltips
    },
    maintainAspectRatio: false
}

function statusCode(i){
    var status ="";
        if (i==="00"){
            status="BERHASIL";
        }else{
            status="GAGAL";
        }
    return status;
}

class Ira extends Component{
    constructor(props){
        super(props);

        this.toggle = this.toggle.bind(this);
        this.loadDataTable = this.loadDataTable.bind(this);
        this.onChange = this.onChange.bind(this);
        this.togglePrimary = this.togglePrimary.bind(this);
        this.SubmitButtonRole = this.SubmitButtonRole.bind(this);
        this.ResetAuto = this.ResetAuto.bind(this);
        this.submitEdit = this.submitEdit.bind(this);
        this.updateData = this.updateData.bind(this);
        this.toggleDanger = this.toggleDanger.bind(this);
        this.deleteRole = this.deleteRole.bind(this);
        this.loadDataRole = this.loadDataRole.bind(this);

        this.state = {
            bar: {
                labels: [],
                datasets: [
                    {
                        label: 'Tipe Channel',
                        backgroundColor: 'rgba(255,99,132,0.2)',
                        borderColor: 'rgba(255,99,132,1)',
                        borderWidth: 1,
                        hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                        hoverBorderColor: 'rgba(255,99,132,1)',
                        data: [],
                    },
                ],
            },

            linechart:{
                labels: [],
                datasets: [
                    {
                        label: 'SUKSES',
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: 'rgba(75,192,192,0.4)',
                        borderColor: 'rgba(75,192,192,1)',
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: 'rgba(75,192,192,1)',
                        pointBackgroundColor: '#fff',
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                        pointHoverBorderColor: 'rgba(220,220,220,1)',
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        data: [],
                    },
                    {
                        label: 'GAGAL',
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: '#FFB6C1',
                        borderColor: '#F08080',
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: '#F08080',
                        pointBackgroundColor: '#fff',
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                        pointHoverBorderColor: 'rgba(220,220,220,1)',
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        data: [],
                    },
                ],
            },

            dataTable: {
                    data: [],
                    totalrow: 0,
            },

            activePage: 1,
            pageActive: 1,
            kartu:"",
            channel:"",
            dropdownJK:[],
            dropdownTC:[],
            start_date:"",
            end_date:"",
            progressbar:[],
            role:{
                data:[],
                total_row: 0,
            },

            primary: false,
            danger: false,
            id:0,
            role_name:"",
            description:"",
            onSubmit: true,
        }
    }

    onChange=(e) => this.setState({
        [e.target.name]:e.target.value
      });

    toggle() {
        this.setState({
          dropdownOpen: !this.state.dropdownOpen,
        });
    }

    componentDidMount(){
        this.loadDataTable(1)
        this.loadDataBarChart()
        this.fetchLine()
        this.loadJenisKartu()
        this.loadTipeChannel()
        this.loadProgress()
        this.loadDataRole(1)
    }

    togglePrimary() {
        this.setState({
          primary: !this.state.primary,
          onSubmit: true
        });
    }

    toggleDanger() {
        this.setState({
          danger: !this.state.danger,
        });
    }

    getDataforDelete(id){
        fetch('http://localhost:5000/ira/'+id,{
            method:'GET',
            header:{
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: null
        }).then((response) => {
            return response.json()
        }).then((response) => {
            this.setState({
                id: id,
                danger: !this.state.danger,
                onSubmit: false
            })
        })
    }

    deleteRole(){
        fetch('http://localhost:5000/ira/delete_role/'+this.state.id, {
            method:'DELETE',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body:null
        }).then(() => {this.toggleDanger()})
          .then(() => {this.loadDataRole()})
    }

    updateData(id){
        fetch('http://localhost:5000/ira/'+id, {
            method:'GET',
            header:{
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: null
        }).then((response) => {
            return response.json()
        }).then((response) => {
            this.setState({
                role_name: response.role_name,
                description: response.description,
                id:id,
                primary: !this.state.primary,
                onSubmit: false
            })
        })
    }

    submitEdit(){
        fetch('http://localhost:5000/ira/edit_role/'+this.state.id, {
            method: 'PUT',
            headers:{
                'Accept' : 'application/json',
			    'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                id: this.state.id,
                role_name: this.state.role_name,
                description: this.state.description,
            })
        }).then(() => {this.togglePrimary()})
          .then(() => {this.loadDataRole()})
          .then(() => {this.ResetAuto()})
    }

    loadDataTable(page){
        this.setState({
            activePage: page
        })
        let handleResponse = function (response){
            return response.json();
        }
        fetch('http://localhost:5000/ira/filter', {
            method: 'POST',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify({
                jkartu: this.state.kartu,
                tchannel: this.state.channel,
                start: this.state.start_date,
                end: this.state.end_date,
                limit: 10,
                pageNumber: page,
            })
        }).then(handleResponse)
          .then( data => {
              this.setState({
                  dataTable: data,
              }, () => console.log(this.state.dataTable))
          });
    }

    loadDataRole(page){
        this.setState({
            pageActive: page
        })
        let handleResponse = function (response){
            return response.json();
        }
        fetch('http://localhost:5000/ira/filterole', {
            method: 'POST',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify({
                limit: 5,
                pageNumber: page,
            })
        }).then(handleResponse)
          .then( data => {
              this.setState({
                  role: data,
              })
          });
    }

    loadDataBarChart() {
        let lineData = Object.assign({}, this.state.bar);
        let handleResponse = function (response) {
            return response.json();
        }
    
        fetch('http://localhost:5000/ira/transactions', {
          method: 'GET',
          headers: {
            'Content-type': 'application/json',
            'Accept': 'application/json'
          },
          body: null
        }).then(handleResponse)
          .then(bar => {
            lineData['labels'] = bar['labels'];
            lineData['datasets'][0]['data'] = bar['data']
            this.setState({
              bar: lineData
            });
        });
    }

    loadProgress(){
        let handleResponse = function (response){
            return response.json();
        }
        
        fetch('http://localhost:5000/ira/progress',{
            method:'GET',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body:null
        }).then(handleResponse)
          .then(prog => {
              this.setState({
                  progressbar: prog,
              }, () => console.log(this.state.progressbar))
          });
    }

    loadJenisKartu(){
        let handleResponse = function (response){
            return response.json();
        }

        fetch('http://localhost:5000/ira/get_jk', {
            method:'GET',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body:null
        }).then(handleResponse)
          .then(drop => {
              this.setState({
                  dropdownJK: drop,
              })
          });
    }

    loadTipeChannel(){
        let handleResponse = function (response){
            return response.json();
        }

        fetch('http://localhost:5000/ira/get_tc', {
            method:'GET',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body:null
        }).then(handleResponse)
          .then(down => {
              this.setState({
                  dropdownTC: down,
              })
          });
    }

    SubmitButtonRole(){
        fetch('http://localhost:5000/ira/insert_role', {
            method:'POST',
            headers:{
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                role_name: this.state.role_name,
                description: this.state.description,
            })
        }).then((response) => {
            return response.json();
        }).then(() => {this.loadDataRole()})
          .then(() => {this.togglePrimary()})
          .then(() => {this.ResetAuto()})
    }

    ResetAuto(){
        this.setState({
            role_name: "",
            description: ""
        })
    }

    fetchLine(){
        let lineData = Object.assign({}, this.state.linechart);
        let handleResponse = function (response){
            return response.json();
        }

        fetch('http://localhost:5000/ira/linetrans', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            },
            body:null
        }).then(handleResponse)
          .then( line => {
            lineData['labels'] = line['labels'];
            lineData['datasets'][0]['data'] = line['data']['sukses']
            lineData['datasets'][1]['data'] = line['data']['gagal']
            
            this.setState({
                linechart:lineData
            })
          })
    }

    render(){
        return(
            <div>
                <Row>
                    <Col>
                        <CardColumns className='cols-2'>
                            <Card>
                                <CardHeader>
                                    <strong>Success Transactions</strong>
                                    <div className="card-header-actions">
                                        <a href="http://www.chartjs.org" className="card-header-action">
                                            <small className="text-muted">docs</small>
                                        </a>
                                    </div>
                                </CardHeader>
                                <CardBody>
                                    <div className="chart-wrapper">
                                        <Bar data={this.state.bar} options={options} />
                                    </div>
                                </CardBody>
                            </Card>
                            <Card>
                                <CardHeader>
                                    <strong>Transactions by Date</strong>
                                    <div className="card-header-actions">
                                        <a href="http://www.chartjs.org" className="card-header-action">
                                            <small className="text-muted">docs</small>
                                        </a>
                                    </div>
                                </CardHeader>
                                <CardBody>
                                    <div className="chart-wrapper">
                                        <Line data={this.state.linechart} options={options} />
                                    </div>
                                </CardBody>                       
                            </Card>
                        </CardColumns>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card>
                            <CardHeader>
                                <strong>Transaction Filtering</strong>
                            </CardHeader>
                            <CardBody>
                                <Form method="POST" encType="multipart/form-data" className="form-horizontal">
                                    <FormGroup row>
                                        <Col md="1">
                                            <Label htmlFor="select">Jenis Kartu</Label>
                                        </Col>
                                        <Col xs="5">
                                            <Input type="select" name="kartu" id="kartu" value={this.state.kartu} onChange={this.onChange}>
                                                <option value="">Pilih Jenis Kartu</option>
                                                {this.state.dropdownJK.map((jeniskartu) =>
                                                    <option key={jeniskartu.jenis_kartu} value={jeniskartu.jenis_kartu}>{jeniskartu.jenis_kartu}</option>
                                                )}
                                            </Input>
                                        </Col>
                                        <Col md="2">
                                            <Label htmlFor="select">Tipe Channel</Label>
                                        </Col>
                                        <Col xs="4">
                                            <Input type="select" name="channel" id="channel" value={this.state.channel} onChange={this.onChange}>
                                                <option value="">Pilih Tipe Channel</option>
                                                {this.state.dropdownTC.map((tipechannel) =>
                                                    <option key={tipechannel.tipe_channel} value={tipechannel.tipe_channel}>{tipechannel.tipe_channel}</option>
                                                )}
                                            </Input>
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Col md="1">
                                            <Label htmlFor="select">Periode</Label>
                                        </Col>
                                        <Col>
                                            <Label htmlFor="exampleInputName2" className="pr-1">Start Date</Label>
                                            <Input type="date" id="start_date" name="start_date" onChange={this.onChange} placeholder="Start Date" value={this.state.start_date} required />
                                        </Col>          
                                        <Col>
                                            <Label htmlFor="exampleInputEmail2" className="pr-1">End Date</Label>
                                            <Input type="date" id="end_date" name="end_date" onChange={this.onChange} placeholder="End Date" value={this.state.end_date} required />
                                        </Col>
                                    </FormGroup>
                                </Form>
                                <Col align="center">
                                    <Button onClick={()=>{this.loadDataTable(this.state.activePage)}} type="submit" size="sm" color="primary"><i className="fa fa-send"></i> Request</Button>
                                </Col>
                                
                                <CardHeader>
                                    <Col align="center">
                                        <strong>All Transactions</strong>
                                    </Col>
                                </CardHeader>
                                <Table hover responsive className="table-outline mb-0 d-none d-sm-table">
                                <thead className="thead-light">
                                    <tr>
                                        <th className="text-center">No</th>
                                        <th>No. Rek</th>
                                        <th>Tipe Channel</th>
                                        <th>Jenis Transaksi</th>
                                        <th>Status Code</th>
                                        <th>TRX Amount</th>
                                        <th>Timestamp</th>
                                        <th>Jenis Kartu</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.dataTable.data.map((table, i)=>(
                                        <tr key={i}>
                                            <td className="text-center">{i + 1 + ((this.state.activePage-1)*10)}</td>
                                            <td>{table.norek}</td>
                                            <td>{table.tipe_channel}</td>
                                            <td>{table.jenis_trx}</td>
                                            <td>{statusCode(table.norek)}</td>
                                            <td>{table.trx_amount}</td>
                                            <td>{table.timestamp}</td>
                                            <td>{table.jenis_kartu}</td>
                                        </tr>
                                    ))}
                                </tbody>
                                </Table>
                                <Pagination
                                    itemClass="page-item"
                                    linkClass="page-link"
                                    activePage={this.state.activePage}
                                    itemsCountPerPage={10}
                                    totalItemsCount={this.state.dataTable.totalrow}
                                    onChange={this.loadDataTable}
                                />
                            </CardBody>
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card>
                            <CardHeader>
                                <strong>Progress Bar</strong>
                            </CardHeader>
                            <CardBody>
                                {this.state.progressbar.map((progress, x)=>(
                                    <div key={x}>
                                        <div className="progress-group">
                                        <div className="progress-group-header">
                                            <i className="icon-user progress-group-icon"></i>
                                            <span className="title">{progress.jenis_kartu}</span>
                                            <span className="ml-auto font-weight-bold">{progress.persentase}%</span>
                                        </div>
                                            <div className="progress-group-bars">
                                            <Progress className="progress-xs" color="warning" value={progress.persentase} />
                                        </div>
                                        </div>
                                    </div>
                                ))}
                            </CardBody>
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card>
                            <CardHeader>
                                <strong>List Roles using Modal</strong>
                                <div className="card-header-actions">
                                    <Button onClick={this.togglePrimary} size="sm" color="primary"><i className="mr-1"></i> Create New</Button>
                                </div>
                            </CardHeader>
                            <CardBody>
                                <Table hover responsive className="table-outline mb-0 d-none d-sm-table">
                                <thead className="thead-light">
                                    <tr>
                                    <th className="text-center">No</th>
                                    <th>Role Name</th>
                                    <th>Description</th>
                                    <th className="text-center">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.role.data.map((params, a)=>(
                                        <tr key={a}>
                                        <td className="text-center">{a + 1 + ((this.state.pageActive-1)*5)}</td>
                                        <td>{params.role_name}</td>
                                        <td>{params.description}</td>
                                        <td className="text-center"><Button onClick={() => {this.updateData(params.id)}} size="sm" color="primary"><i className="fa fa-send"></i>Edit</Button>&nbsp;
                                                                    <Button onClick={() => {this.getDataforDelete(params.id)}} size="sm" color="danger"><i className="fa fa-send">Delete</i></Button>
                                        </td>
                                    </tr>
                                    ))}
                                </tbody>
                                </Table>
                                <Pagination
                                    itemClass="page-item"
                                    linkClass="page-link"
                                    activePage={this.state.pageActive}
                                    itemsCountPerPage={5}
                                    totalItemsCount={this.state.role.total_row}
                                    onChange={this.loadDataRole}
                                />
                            </CardBody>
                        </Card>
                    </Col>
                </Row>

                {/* Modal Create Data */}
                <Modal isOpen={this.state.primary} toggle={this.togglePrimary} className={'modal-primary ' + this.props.className}>
                    <ModalHeader toggle={this.togglePrimary}>
                        <RcIf if={this.state.onSubmit}>
                            Create New Data of Roles
                            <RcElse>
                                    Edit Data Roles
                            </RcElse>
                        </RcIf>
                    </ModalHeader>
                        <ModalBody>
                            <Form method="POST" encType="multipart/form-data" className="form-horizontal">
                                <FormGroup row>
                                    <Col md="3">
                                        <Label htmlFor="text-input">Role Name</Label>
                                    </Col>
                                    <Col xs="12" md="9">
                                        <Input type="text" className="form-control-warning" id="role_name" name="role_name" onChange={this.onChange} placeholder="Role Name" value={this.state.role_name} required/>
                                        <FormText color="muted">Input Role Name Here</FormText>
                                    </Col>
                                </FormGroup>
                                <FormGroup row>
                                    <Col md="3">
                                        <Label htmlFor="text-input">Description</Label>
                                    </Col>
                                    <Col xs="12" md="9">
                                        <Input type="textarea" className="form-control-warning" id="description" name="description" onChange={this.onChange} placeholder="Description Name" value={this.state.description} required/>
                                        <FormText color="muted">Input Your Description Here</FormText>
                                    </Col>
                                </FormGroup>
                            </Form>
                    </ModalBody>
                    <ModalFooter>
                        <RcIf if={this.state.onSubmit}>
                            <Button color="primary" onClick={this.SubmitButtonRole} type="submit">Save</Button>
                            <RcElse>
                                <Button color="primary" onClick={this.submitEdit}>Update</Button>
                            </RcElse>
                        </RcIf>
                        {' '}
                            <Button color="secondary" onClick={this.togglePrimary}>Cancel</Button>
                    </ModalFooter>
                </Modal>
                
                {/* Modal Delete Data */}
                <Modal isOpen={this.state.danger} toggle={this.toggleDanger} className={'modal-danger ' + this.props.className}>
                    <ModalHeader toggle={this.toggleDanger}>Delete Role</ModalHeader>
                        <ModalBody>
                            Are you sure wanna delete this Role?
                        </ModalBody>
                    <ModalFooter>
                        <Button color="danger" onClick={this.deleteRole}>Of Course</Button>{' '}
                        <Button color="secondary" onClick={this.toggleDanger}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        )
    }
}

export default Ira;