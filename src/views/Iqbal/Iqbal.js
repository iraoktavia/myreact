import React, { Component } from 'react';
import { Row, Col, Card ,  Form , FormGroup , Label , CardBody , CardHeader , Button, Table ,Input, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { Bar, Pie, Scatter , Line } from 'react-chartjs-2';
import Pagination from "react-js-pagination";
import RcIf, {RcElse} from 'rc-if';

const bar = {
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    datasets: [
      {
        label: 'Frequency',
        backgroundColor: 'rgba(32, 168, 216,0.2)',
        borderColor: 'rgba(32, 168, 216,1)',
        borderWidth: 1,
        hoverBackgroundColor: 'rgba(32, 168, 216,0.4)',
        hoverBorderColor: 'rgba(32, 168, 216,1)',
        data: [65, 59, 80, 81, 56, 55, 40],
      },
    ],
  };
const line = {
  labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
  datasets: [
    {
      label: 'Susses',
      fill: false,
      lineTension: 0.1,
      backgroundColor: 'rgba(75,192,192,0.4)',
      borderColor: 'rgba(75,192,192,1)',
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: 'rgba(75,192,192,1)',
      pointBackgroundColor: '#fff',
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: 'rgba(75,192,192,1)',
      pointHoverBorderColor: 'rgba(220,220,220,1)',
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data: [65, 59, 80, 81, 56, 55, 40],
    },
	{
      label: 'Fail',
      fill: false,
      lineTension: 0.1,
      backgroundColor: 'rgba(242, 38, 19, 1)',
      borderColor: 'rrgba(242, 38, 19, 1)',
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: 'rgba(242, 38, 19, 1)',
      pointBackgroundColor: '#fff',
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: 'rgba(242, 38, 19, 1)',
      pointHoverBorderColor: 'rgba(242, 38, 19, 1)',
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data: [21, 1, 2, 87, 90, 45, 40],
    },
  ],
};

class Iqbal extends Component {
  constructor(props) {
      super(props);

      this.state = {
        soal1 : bar,
		soal2 : line,
		soal3 : {
			data: [],
			totalRows: 0
		},
		activePage:1,
		start_date : '2019-01-01',
		end_date : '2025-12-12',
		jenis_kartu : 'SEMUA',
		tipe_channel: 'SEMUA',
		modal : false,
		popUpAdd : false,
		id : 0,
		role_name : '',
		description : '',
		data_role : {
			data:[],
			jumlah_row:0
		},
		buttonSubmit : true
	}
	this.resetRole = this.resetRole.bind(this);
	this.addRole = this.addRole.bind(this);
	this.editRole = this.editRole.bind(this);
	this.addButton = this.addButton.bind(this);
	this.editButton = this.editButton.bind(this);
	this.getDataRoles = this.getDataRoles.bind(this);
	this.fetchSoalNo3 = this.fetchSoalNo3.bind(this);
	this.toggle = this.toggle.bind(this);
	this.closePopUp = this.closePopUp.bind(this);
  }

  onChange = (input) => {
	  this.setState({
        [input.target.name]: input.target.value
      }, () => {
		  this.fetchSoalNo3(1)
	  });
  }

  componentDidMount(){
	this.getDataRoles();
	this.fetchSoalNo1();
	this.fetchSoalNo2()	;
	this.fetchSoalNo3(1);
  }
  
  toggle() {
    this.setState({
      modal: !this.state.modal,
    });
  }

  addButton(){
	this.setState({
		popUpAdd : !this.state.popUpAdd,
		buttonSubmit : true
	})
  }

  closePopUp(){
	this.setState({
		popUpAdd : !this.state.popUpAdd,
	});
	this.resetRole();
  }

  editButton(id){
	fetch('http://localhost:5000/iqbal/roles/'+id , {
		method:'GET',
		headers:{
			'Accept' : 'application/json',
			'Content-Type' : 'application/json'
		},
		body : null
	}).then((response) => {return response.json()})
	.then((data) => {
		this.setState({
		role_name : data.role_name,
		description : data.description,
		id : id,
		popUpAdd : !this.state.popUpAdd,
		buttonSubmit : false
		})
	})
  }

  editRole(){
	fetch('http://localhost:5000/iqbal/roles/update',{
		method:'PUT',
		headers:{
			'Accept' : 'application/json',
			'Content-Type' : 'application/json'
		},
		body : JSON.stringify({
			id : this.state.id,
			role_name : this.state.role_name,
			description : this.state.description
		})
	}).then (() => {this.addButton()})
  .then(()=> {this.getDataRoles()})
  .then(()=> {this.resetRole()})
  }
  resetRole(){
	  this.setState({
		  id : 0,
		  role_name : '',
		  description : ''
	  })
  }
  
  addRole(){
	fetch('http://localhost:5000/iqbal/roles/insert' , {
		method:'POST',
		headers:{
			'Accept' : 'application/json',
			'Content-Type' : 'application/json'
		},
		body: JSON.stringify({
            role_name: this.state.role_name,
            description: this.state.description,
          })
  }).then (() => {this.addButton()})
  .then(()=> {this.getDataRoles()})
  .then(()=> {this.resetRole()})
  }
  getDataRoles(){
	  fetch('http://localhost:5000/iqbal/roles/' , {
		  method:'POST',
		  headers:{
			
				'Accept' : 'application/json',
				'Content-Type' : 'application/json'
		  },
		  body: JSON.stringify({
            limit: 5,
            page: 1,
          })
	  }).then((response) => {return response.json()})
	  .then((callBack) => {
		  console.log(callBack);
		  this.setState({
			  'data_role' : callBack
		  });
	  })
  }
  
  fetchSoalNo1(){
        fetch('http://localhost:5000/iqbal/top-10-fail',{
            method : 'GET',
            headers:{
				
                'Accept' : 'application/json',
                'Content-type' : 'application/json'
            }
        })
        .then((response) => {return response.json()})
        .then((isi) => {
            console.log(isi)
			var bar = {
            labels: [],
            datasets: [
              {
                label: 'Frequency',
                backgroundColor: 'rgba(32, 168, 216,0.2)',
                borderColor: 'rgba(32, 168, 216,1)',
                borderWidth: 1,
                hoverBackgroundColor: 'rgba(32, 168, 216,0.4)',
                hoverBorderColor: 'rgba(32, 168, 216,1)',
                data:[],
              },	
            ],
          };
		bar.labels = isi['label']
		bar.datasets[0].data = isi['data']
           

        this.setState({'soal1':bar});
        });
    }
	fetchSoalNo2(){
        console.log('x');
        fetch('http://localhost:5000/iqbal/sumhour',{
            method : 'GET',
            headers:{
				
                'Accept' : 'application/json',
                'Content-type' : 'application/json'
            }
        })
        .then((response) => {return response.json()})
        .then((isi) => {
            console.log(isi)
			const line = {
				  labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
				  datasets: [
					{
					  label: 'Susses',
					  fill: false,
					  lineTension: 0.1,
					  backgroundColor: 'rgba(75,192,192,0.4)',
					  borderColor: 'rgba(75,192,192,1)',
					  borderCapStyle: 'butt',
					  borderDash: [],
					  borderDashOffset: 0.0,
					  borderJoinStyle: 'miter',
					  pointBorderColor: 'rgba(75,192,192,1)',
					  pointBackgroundColor: '#fff',
					  pointBorderWidth: 1,
					  pointHoverRadius: 5,
					  pointHoverBackgroundColor: 'rgba(75,192,192,1)',
					  pointHoverBorderColor: 'rgba(220,220,220,1)',
					  pointHoverBorderWidth: 2,
					  pointRadius: 1,
					  pointHitRadius: 10,
					  data: [65, 59, 80, 81, 56, 55, 40],
					},
					{
					  label: 'Fail',
					  fill: false,
					  lineTension: 0.1,
					  backgroundColor: 'rgba(242, 38, 19, 1)',
					  borderColor: 'rgba(242, 38, 19, 1)',
					  borderCapStyle: 'butt',
					  borderDash: [],
					  borderDashOffset: 0.0,
					  borderJoinStyle: 'miter',
					  pointBorderColor: 'rgba(242, 38, 19, 1)',
					  pointBackgroundColor: '#fff',
					  pointBorderWidth: 1,
					  pointHoverRadius: 5,
					  pointHoverBackgroundColor: 'rgba(242, 38, 19, 1)',
					  pointHoverBorderColor: 'rgba(242, 38, 19, 1)',
					  pointHoverBorderWidth: 2,
					  pointRadius: 1,
					  pointHitRadius: 10,
					  data: [21, 1, 2, 87, 90, 45, 40],
					},
				  ],
				};
		line.labels = isi['label']
		line.datasets[0].data = isi['data']['sukses']
		line.datasets[1].data = isi['data']['gagal']
           

        this.setState({'soal2':line});
        });
    }
	fetchSoalNo3(pageNumber) {
        fetch('http://localhost:5000/iqbal/', {
          method: 'POST',
          headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            limit: 10,
            page: pageNumber,
			start_date : this.state.start_date,
			end_date :  this.state.end_date,
			jenis_kartu : this.state.jenis_kartu,
			tipe_channel : this.state.tipe_channel,
          })
        }).then((response) => {
          return response.json();
        }).then((data) => {
          console.log(data);
		  this.setState({'soal3': data , 'activePage':pageNumber});
        });
      }
  render() {

    return (
      <div className="animated fadeIn">
		<Row>
			<Col>
				<Card>
					<CardHeader>
						SOAL NO 1
					</CardHeader>
					<CardBody>
						<div className="chart-wrapper">
                            <Bar data={this.state.soal1} />
                        </div>
					</CardBody>
				</Card>
			</Col>
		</Row>
		<Row>
			<Col>
				<Card>
					<CardHeader>
						SOAL NO 2
					</CardHeader>
					<CardBody>
						<div className="chart-wrapper">
                            <Line data={this.state.soal2} />
                        </div>
					</CardBody>
				</Card>
			</Col>
		</Row>
		<Row>
				
                    <Col>
                        <Card>
                            <CardBody>
							<Row>
								<Col xs="4">
									Jenis Kartu
								</Col>
								<Col xs="8">
									<Input type="select" name="jenis_kartu" id="jenis_kartu" onChange={this.onChange}>
                                                <option value="SEMUA">SEMUA</option>
                                                <option value="SIMPEDES">SIMPEDES</option>
                                                <option value="BRITAMA">BRITAMA</option>
                                                <option value="PRIORITAS">PRIORITAS</option>
                                    </Input>
								</Col>
							</Row>
							<Row>
								<Col xs="4">
									Tipe Channel
								</Col>
								<Col xs="8">
									<Input type="select" name="tipe_channel" id="tipe_channel" onChange={this.onChange}>
                                                <option value="SEMUA">SEMUA</option>
                                                <option value="ATM">ATM</option>
                                                <option value="EDC">EDC</option>
                                                <option value="INTERNET BANKING">INTERNET BANKING</option>
                                                <option value="SMS BANKING">SMS BANKING</option>
                                    </Input>
								</Col>
							</Row>
							<Row>
								<Col xs="4">
									Tipe Channel
								</Col>
								<Col xs="4">
									<Input type="date" id="start_date" name="start_date" value={this.state.start_date} onChange={this.onChange} required />
								</Col>
								<Col xs="4">
									<Input type="date" id="end_date" name="end_date" value={this.state.end_date} onChange={this.onChange} required />
								</Col>
							</Row>
							<br/>
                            <Table hover responsive className="table-outline mb-0 d-none d-sm-table">
                            <thead className="thead-light">
                                <tr>
                                    <th className="text-center">No</th>
                                    <th className="text-center">Jenis Kartu</th>
                                    <th className="text-center">Jenis Trasaction</th>
                                    <th className="text-center">NO Rekeneing</th>
                                    <th className="text-center">Status Code</th>
                                    <th className="text-center">Tanggal</th>
                                    <th className="text-center">Tipe Channel</th>
                                    <th className="text-center">Jumlah transaksi</th>
                                    <th className="text-center">Tunai</th>
                                </tr>
                            </thead>
                            <tbody>
                                { this.state.soal3.data.map((row, index) => {
                                return (
                                    <tr key={index}>
                                    <td>{index + 1 + ((this.state.activePage-1)*10)}</td>
                                    <td>{row.jenis_kartu}</td>
									<td>{row.jenis_trx}</td>
                                    <td>{row.norek}</td>
                                    <td>{row.status_code}</td>
                                    <td>{row.timestamp}</td>
                                    <td>{row.tipe_channel}</td>
                                    <td>{row.trx_amount}</td>
                                    <td>{row.tunai}</td>
                                    </tr>
                                )
                                }) }
                            </tbody>
							</Table>
							<Pagination
							  itemClass="page-item"
							  linkClass="page-link"										 
							  activePage={this.state.activePage}
							  itemsCountPerPage={10}
							  totalItemsCount={this.state.soal3.totalRows}
							  onChange={this.fetchSoalNo3}

							/>
                            </CardBody>
                        </Card>
						</Col>
                </Row>
				<Row>
					<Col>
						<Card>
							<CardHeader>
								Modals 
							</CardHeader>
							<CardBody>
								 <Col col="6" sm="4" md="2" xl className="mb-3 mb-xl-0">
									<Button block color="success" onClick={this.toggle}>Show</Button>
									<Modal isOpen={this.state.modal} toggle={this.toggle}>
									  <ModalHeader toggle={this.toggle}>Search Tabel</ModalHeader>
									  <ModalBody>
										Jenis Kartu : {this.state.jenis_kartu}<br/>
										Tipe Channel : {this.state.tipe_channel}<br/>
										Start Date : {this.state.start_date}<br/>
										End Date : {this.state.end_date}<br/>
									  </ModalBody>
									  <ModalFooter>
										<Button color="primary" onClick={this.toggle}>Do Something</Button>{' '}
										<Button color="secondary" onClick={this.toggle}>Cancel</Button>
									  </ModalFooter>
									</Modal>
								 </Col>
							</CardBody>
						</Card>
					</Col>
				</Row>
				<Row>
					<Col>
					<Card>
						<CardHeader>
							CRUD ROLE USING MODAL
						</CardHeader>
						<CardBody>
							<Row>
								<Col xs='11'>
								</Col>
								<Col xs='1'>
								<Button block color="primary" onClick={this.addButton} icon="fa fa-user"><i className="fa fa-plus-circle"></i></Button>
								</Col>
							</Row>
							<br/>
							<Row>
							<Table hover responsive className="table-outline mb-0 d-none d-sm-table">
                            <thead className="thead-light">
                                <tr>
                                    <th className="text-center">No</th>
                                    <th className="text-center">ROLE NAME</th>
                                    <th className="text-center">Description</th>
									<th className="text-center">ACTION</th>
                                </tr>
                            </thead>
                            <tbody>
                                { this.state.data_role.data.map((row, index) => {
                                return (
								<tr key={row.id}>
									<td>{index + 1 + ((this.state.activePage-1)*10)}</td>
									<td>{row.role_name}</td>
									<td>{row.description}</td>
									<td><Button type="button" color="primary" onClick={() => this.editButton(row.id)}><i className="fa fa-pencil-square-o"></i> EDIT</Button> || <Button type="button" color="danger" onClick={(() => this.onDelete(row.id))}><i className="fa fa-trash-o"></i>HAPUS</Button> </td>
								</tr>
                                )
                                }) }
                            </tbody>
							</Table>
							</Row>
						</CardBody>
					</Card>
					</Col>
				</Row>
				<Col col="6" sm="4" md="2" xl className="mb-3 mb-xl-0">
					<Modal isOpen={this.state.popUpAdd} toggle={this.closePopUp}>
					  <ModalHeader toggle={this.closePopUp}>
					  <RcIf if={this.state.buttonSubmit}>
							ADD Roles
						</RcIf>
						<RcIf if={!this.state.buttonSubmit}>
							Edit Roles 
						</RcIf>
					    </ModalHeader>
					  <ModalBody>
						<FormGroup>
						  <Label htmlFor="role_name">Role Name</Label>
						  <Input type="text" id="role_name" name="role_name" value={this.state.role_name} placeholder="Enter role name" onChange={this.onChange} required />
						</FormGroup>
						<FormGroup>
						  <Label htmlFor="description">Description</Label>
						  <Input type="textarea" id="description" name="description" value={this.state.description} onChange={this.onChange}/>
						</FormGroup>
					  </ModalBody>
					  <ModalFooter>
						<RcIf if={this.state.buttonSubmit}>
							<Button color="primary" onClick={this.addRole}> Save </Button>
						</RcIf>
						<RcIf if={!this.state.buttonSubmit}>
							<Button color="primary" onClick={this.editRole}> Edit </Button>
						</RcIf>
						{' '}
						<Button color="danger" onClick={this.resetRole}>Reset</Button>
					  </ModalFooter>
					</Modal>
				 </Col>
						
      </div>
    );
  }
}

export default Iqbal;
