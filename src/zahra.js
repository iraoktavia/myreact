import React, {Component} from 'react';
import {Bar, Line} from 'react-chartjs-2';
import{
    Button,
    // ButtonGroup,
    // ButtonToolbar,
    Card,
    CardBody,
    //CardFooter,
    CardHeader,
    CardColumns,
    //CardTitle,
    Col,
    Progress,
    Row,
    Table,
    Form,
    FormGroup,
    FormText,
    Input,
    Label,
} from 'reactstrap';
import Pagination from 'react-js-pagination'; 
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';

function statusCode(i){
    var status="";
    if(i=='00'){
        status="BERHASIL";
    }else{
        status="GAGAL";
    }

    return status;
}

const options = {
    tooltips: {
      enabled: false,
      custom: CustomTooltips
    },
    maintainAspectRatio: false
  };
class Zahra extends Component{
    constructor(props){
        super(props);
        this.state = {
            bar: {
              labels: [],
              datasets: [
                {
                  label: '',
                  backgroundColor: 'rgba(255,99,132,1)',
                  borderColor: 'rgba(255,99,132,1)',
                  borderWidth: 1,
                  hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                  hoverBorderColor: 'rgba(255,99,132,1)',
                  data: [],
                },
              ],
            },
            line: {
                labels: [],
                datasets: [
                  {
                    label: 'Berhasil',
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: 'rgba(75,192,192,0.4)',
                    borderColor: 'rgba(75,192,192,1)',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: 'rgba(75,192,192,1)',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                    pointHoverBorderColor: 'rgba(220,220,220,1)',
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: [],
                  },
                  {
                    label: 'Gagal',
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: 'rgba(255,99,132,1)',
                    borderColor: 'rgba(255,99,132,1)',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: 'rgba(255,99,132,1)',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: 'rgba(255,99,132,1)',
                    pointHoverBorderColor: 'rgba(220,220,220,1)',
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: [],
                  },
                ],
              },
            jenis_kartu:'',
            tipe_channel:'',
            awal:'',
            akhir:'',
            tabel:{totalrow:0,data:[]},
            activePage:1,
        };
        this.onChange=this.onChange.bind(this);
        this.loadDataTabelChart=this.loadDataTabelChart.bind(this)
    }

    onChange(e){
        this.setState({
            [e.target.name]:e.target.value
        });
    }

    componentDidMount(){
        this.loadDataBarChart();
        this.loadDataLineChart();
        this.loadDataTabelChart(1);
    }
    
    loadDataBarChart(){
        let barData=Object.assign({}, this.state.bar);
        let handleResponse = function (response){
            return response.json();
        }
      
        fetch('http://localhost:5000/zahra/jenis_trans_gagal',{
            method:'GET',
            headers:{
                'Content-type':'application/json',
                'Accept':'application/json'
            },
            body:null
        }).then(handleResponse)
        .then(x=>{
            barData['labels']=x['labels'];
            barData['datasets'][0]['data']=x['data']
            this.setState({
                bar:barData
            })
        });
    }

    loadDataLineChart(){
        let lineData=Object.assign({}, this.state.line);
        let handleResponse = function (response){
          return response.json();
        }
        
        fetch('http://localhost:5000/zahra/showtrans',{
            method:'GET',
            headers:{
                'Content-type':'application/json',
                'Accept':'application/json'
            },
            body:null
        }).then(handleResponse)
        .then(x=>{
            // console.log(x)
            lineData['labels']=x['labels'];
            lineData['datasets'][0]['data']=x['data']['berhasil']
            lineData['datasets'][1]['data']=x['data']['gagal']
            
            // lineData['labels']=x['labels'];
            // lineData['datasets'][0]['data']=x['data'] 
            this.setState({
              line:lineData
            })
        }); 
    }

    loadDataTabelChart(page){
        this.setState({
            activePage:page
        })
        
        let handleResponse = function (response){
            return response.json();
        }
    
        fetch('http://localhost:5000/zahra/showalltrans',{
            method:'POST',
            headers:{
                'Content-type':'application/json',
                'Accept':'application/json'
            },
            body:JSON.stringify({
                kartu:this.state.jenis_kartu,
                channel:this.state.tipe_channel,
                awal:this.state.awal,
                akhir:this.state.akhir,
                limit:10,
                pageNumbers:page,
              })
        }).then(handleResponse)
        .then(x=>{
          this.setState({
            tabel:x
          })
        });
    }

    render(){
        return(
            <div className="animated fadeIn">
                <Row>
                    <CardColumns className="cols-2">
                        <Card>
                            <CardHeader>Transaksi Gagal</CardHeader>
                            <CardBody>
                                <div className="chart-wrapper">
                                    <Bar data={this.state.bar} options={options} />
                                </div>
                            </CardBody>
                        </Card>
                        <Card>
                            <CardHeader>Transaksi Tiap Jam</CardHeader>
                            <CardBody>
                                <div className="chart-wrapper">
                                    <Line data={this.state.line} options={options} />
                                </div>
                            </CardBody>
                        </Card>
                    </CardColumns>
                </Row>
                <Row>
                    <Col>
                        <Card>
                            <CardHeader>Filter Transaksi</CardHeader>
                            <CardBody>
                                <Form method="post" className="form-horizontal">
                                    <FormGroup row>
                                        <Col md="2">
                                            <Label htmlFor="jenis_kartu">Jenis Kartu</Label>
                                        </Col>
                                        <Col xs="3">
                                            <Input type="select" name="jenis_kartu" id="jenis_kartu" value={this.state.jenis_kartu} onChange={this.onChange}>
                                                <option value="">-Pilih Jenis Kartu-</option>
                                                <option value="BRITAMA">BRITAMA</option>
                                                <option value="PRIORITAS">PRIORITAS</option>
                                                <option value="SIMPEDES">SIMPEDES</option>
                                            </Input>
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Col md="2">
                                            <Label htmlFor="tipe_channel">Tipe Channel</Label>
                                        </Col>
                                        <Col xs="3">
                                            <Input type="select" name="tipe_channel" id="tipe_channel" value={this.state.tipe_channel} onChange={this.onChange}>
                                                <option value="">-Pilih Tipe Channel-</option>
                                                <option value="ATM">ATM</option>
                                                <option value="EDC">EDC</option>
                                                <option value="INTERNET BANKING">INTERNET BANKING</option>
                                                <option value="SMS BANKING">SMS BANKING</option>
                                            </Input>
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Col md="2">
                                            <Label htmlFor="timestamp">Periode</Label>
                                        </Col>
                                        <Col xs="3">
                                            <FormText className="help-block">Start Date</FormText>
                                            <Input type="date" value={this.state.awal} id="awal" name="awal" onChange={this.onChange} placeholder="mm/dd/yyyy" />
                                        </Col>
                                        <Col xs="3">
                                            <FormText className="help-block">End Date</FormText>
                                            <Input type="date" value={this.state.akhir} id="akhir" name="akhir" onChange={this.onChange} placeholder="mm/dd/yyyy" />
                                        </Col>
                                    </FormGroup>
                                </Form>
                                <Col align="center">
                                    <Button type="submit" size="sm" color="primary" onClick={()=>this.loadDataTabelChart(this.state.activePage)}><i className="fa fa-send"></i> Find</Button>
                                </Col>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Card>
                            <CardHeader>Transaksi</CardHeader>
                            <CardBody>
                                <Table hover responsive className="table-outline mb-0 d-none d-sm-table">
                                    <thead className="thead-light">
                                        <tr>
                                            <th className="text-center">No.</th>
                                            <th className="text-center">No. Rekening</th>
                                            <th className="text-center">Tipe Channel</th>
                                            <th className="text-center">Jenis Transaksi</th>
                                            <th className="text-center">Status</th>
                                            <th className="text-center">Nominal</th>
                                            <th className="text-center">Waktu Kejadian</th>
                                            <th className="text-center">Jenis Kartu</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.tabel.data.map((tabel,index)=>(
                                            <tr key={index}>
                                                <td className="text-center">{index+1+ ((this.state.activePage-1)*10)}</td>
                                                <td className="text-center">{tabel.norek}</td>
                                                <td className="text-center">{tabel.tipe_channel}</td>
                                                <td className="text-center">{tabel.jenis_trx}</td>
                                                <td className="text-center">{statusCode(tabel.status_code)}</td>
                                                <td className="text-center">{tabel.trx_amount}</td>
                                                <td className="text-center">{tabel.timestamp}</td>
                                                <td className="text-center">{tabel.jenis_kartu}</td>
                                            </tr>
                                        ))} 
                                    </tbody>
                                </Table>
                                <Pagination
                                    itemClass="page-item"
                                    linkClass="page-link"
                                    activePage={this.state.activePage}
                                    itemsCountPerPage={10}
                                    totalItemsCount={this.state.tabel.totalrow}
                                    onChange={this.loadDataTabelChart}
                                />
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default Zahra;