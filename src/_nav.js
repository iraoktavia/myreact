export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer',
      badge: {
        variant: 'info',
        text: 'NEW',
      },
    },
    {
      name: 'Faisal',
      url: '/faisal',
      icon: 'icon-people'
    },
    {
      name: 'Irin',
      url: '/irin',
      icon: 'icon-credit-card',
    },
    {
      name: 'Anissa LF',
      url: '/anissa',
      icon: 'icon-user-female',
    },
    {
      name: 'Dashboard Zahra',
      url: '/dashboard_zahra',
      icon: 'icon-pie-chart',
    },
    {
      name: 'Ira',
      url: '/ira',
      icon: 'icon-speedometer',
    },
	  {
		name: 'Iqbal',
		url: '/Iqbal',
		icon: 'fa fa-user',
		badge: {
        variant: 'info',
        text: 'Ganteng',
      },
	  },
  ],
};
