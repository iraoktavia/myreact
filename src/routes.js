import React from 'react';
import DefaultLayout from './containers/DefaultLayout';
const Dashboard = React.lazy(() => import('./views/Dashboard'));
const Faisal = React.lazy(() => import('./views/Faisal/Faisal'));
const Charts = React.lazy(() => import('./views/Transactions/irin_transactions'));
const Anissa = React.lazy(() => import('./views/Anissa/Anissa'));
const Zahra = React.lazy(() => import('./zahra'));
const Ira = React.lazy(() => import('./views/Transactions/ira'));
const Iqbal = React.lazy(() => import('./views/Iqbal/Iqbal'));
const Transactions = React.lazy(() => import('./views/Transactions/irin_transactions'));

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/', exact: true, name: 'Home', component: DefaultLayout },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/faisal', name: 'Faisal', component: Faisal},
  // { path: '/transactions', name: 'Transactions', component: Transactions },
  { path: '/irin', name: 'Transactions', component: Charts },
  { path: '/anissa', name: 'Anissa', component: Anissa},
  { path: '/dashboard_zahra', name: 'Dashboard Zahra', component: Zahra },
  { path: '/ira', name: 'Dashboard Ira', component: Ira },
  { path: '/Iqbal' , name:"Iqbal" , component:Iqbal }, 
];

export default routes;
