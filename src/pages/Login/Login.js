import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  Button,
  Card,
  CardBody, 
  CardGroup, 
  Col, 
  Container, 
  Form, 
  Input, 
  InputGroup, 
  InputGroupAddon, 
  InputGroupText, 
  Row
}from 'reactstrap';
import { userActions } from '../_actions/user.actions';

class Login extends Component {
  constructor(props) {
    super(props);

    //reset login status
    this.props.dispatch(userActions.logout());

    this.state = {
      loginForm: {
        personal_number: '',
        password: '',
      },
      submitted: false 
    };

    this.onChangeHandler = this.onChangeHandler.bind(this);
  }

  // ketika nilai input berubah
  onChangeHandler(event) {
    let loginForm = {...this.state.loginForm};
    loginForm[event.target.name] = event.target.value;

    this.setState({
      loginForm,
    });
  }

  handleSubmit(e){
    e.preventDefault();

    this.setState({ submitted: true });
    const { loginForm } = this.state;
    const { dispatch } = this.props;
    if ( loginForm ){
      dispatch(userActions.login(loginForm));
    }
  }

  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="5">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form>
                      <h1 className="text-center">Login</h1>
                      <p className="text-muted text-center">Sign In to your account</p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" placeholder="Personal Number" name="personal_number" onChange={this.onChangeHandler} />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="password" placeholder="Password" name="password" onChange={this.onChangeHandler} />
                      </InputGroup>
                      <Row>
                        <Col xs="6">
                          <Button color="primary" className="px-4">Login</Button>
                        </Col>
                        <Col xs="6" className="text-right">
                          <Button color="link" className="px-0">Forgot password?</Button>
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Login;
